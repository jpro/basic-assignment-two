import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  username:string = "";
  allowButtonClick:boolean = false;

  constructor() {}

  ngOnInit() {
  }

  onClearUsername() {
    this.allowButtonClick = !this.allowButtonClick;
    this.username = "";
  }

  isUsernameEmpty() {
    return this.username.length == 0;
  }

}
