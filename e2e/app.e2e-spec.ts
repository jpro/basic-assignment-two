import { BasicsAssignmentTwoPage } from './app.po';

describe('basics-assignment-two App', () => {
  let page: BasicsAssignmentTwoPage;

  beforeEach(() => {
    page = new BasicsAssignmentTwoPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
